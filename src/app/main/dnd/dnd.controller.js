(function ()
{
    'use strict';

    angular
        .module('app.dnd')
        .controller('DndController', DndController);

    function DndController($scope)
    {

      $scope.models = {
        selected: null,
        lists: {"Todo": [], "In Progress": [], "Done" : []}
      };

      $scope.$watch('models', function(model) {
        $scope.modelAsJson = angular.toJson(model, true);
      }, true);

      $scope.addCardModel = function(listName){
        $scope.CurrentListName = listName;
        $scope.cardName = '';
      };

      $scope.saveCard = function (list, val) {
        if($scope.addCardForm.$valid){
          $scope.models.lists[list].push({label: val});
          $("#addModal").modal('hide');
        }else{
          alert("please add card name");
        }
      };

      $scope.removeCard = function(list,index){
        $scope.models.lists[list].splice(index,1);
      };
    }
})();
