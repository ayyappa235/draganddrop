(function ()
{
    'use strict';

    angular
        .module('app.dnd', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider)
    {
        // State
        $stateProvider
            .state('dnd', {
                url    : '/dnd',
                views  : {
                  'main'         : {
                    templateUrl: "app/main/dnd/dnd.html",
                    controller : 'DndController as vm'}
                }
            });

        // Api

    }
})();
