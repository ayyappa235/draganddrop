(function ()
{
    'use strict';

    angular
        .module('noBroker')
        .factory('api', apiService);

    /** @ngInject */
    function apiService($resource)
    {

        api.baseUrl = 'app/data/';

        return api;
    }

})();
