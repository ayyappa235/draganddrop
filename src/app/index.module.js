(function ()
{
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('noBroker', [

          'ui.router',
            // Sample
            'app.dnd',
          'dndLists'
        ]);
})();
